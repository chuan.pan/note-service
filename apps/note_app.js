const express = require('express');
const createError = require('http-errors');
const bodyParser = require('body-parser');
const app = express();
const notesRouter = require('../routes/notes');
const indexRouter = require('../routes/index');
const cors = require('cors');

const corsOptions = {
    origin: '*'
};

app.use(cors(corsOptions));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));
app.set('view engine', 'ejs');

app.use('/', indexRouter);
app.use('/notes', notesRouter);

// catch 404 and forward to error handler
app.use(function (req, res, next) {
    next(createError(404));
});

// error handler
app.use(function (err, req, res, next) {
    res.status(err.status || 500).json({message: err.message});
});
module.exports = app;