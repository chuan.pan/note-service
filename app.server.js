const note_app_service = require('./apps/note_app');
const note_app_service_port = 8080;
note_app_service.listen(note_app_service_port);
console.log('Note App listening on http://localhost:' + note_app_service_port);
