# Simple Note App

This simple Note app in built with node.js

For running the server please use the following command.

```bash
node app.local.js
```

## Unit Test
The project uses [jasmine]() as the unit test framework.
For running the tests please execute the following command under the project folder.
```bash
jasmine
```

## Storage

All the data is stored in local cache, so it will be reset when every time the server is rebooted.

## Authors
* **Chuan Pan** - *Initial work* - [LinkedIn](https://www.linkedin.com/in/chuanpan/)

## License

This project is licensed under the MIT License