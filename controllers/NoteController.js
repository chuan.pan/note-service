"use strict";
const shortid = require('shortid');
const NodeCache = require("node-cache");
const cache = new NodeCache();
const Note = require('../models/Note');

module.exports = {
    getNotes: (callback) => {
        cache.keys((err, keys) => {
            if (!err) {
                let notes = [];
                for (let key of keys) {
                    notes.push(cache.get(key));
                }
                callback(err, notes);
            } else {
                callback(err, null);
            }
        });
    },
    addAndGet: (title, content, callback) => {
        const note = new Note();
        note.id = shortid.generate();
        note.title = title;
        note.content = content;
        const now = new Date();
        note.created = now;
        note.updated = now;

        cache.set(note.id, note, (err, success) => {
            if (!err && success) {
                callback(err, note.id);
            } else {
                callback(err, null);
            }
        });
    },
    getNoteByID: (id, callback) => {
        let note = cache.get(id);
        callback(null, note);
    },
    updateNoteByID: (id, title, content, callback) => {
        let currentNote = cache.get(id);
        let updateNote = Object.assign({}, currentNote);
        updateNote.title = title;
        updateNote.content = content;
        updateNote.updated = new Date();
        cache.set(id, updateNote, (err, success) => {
            if (!err && success) {
                callback(null, id);
            } else {
                callback(err, null);
            }
        });
    },
    deleteNoteByID: (id, callback) => {
        cache.del(id, (err, count) => {
            if (count === 0) {
                callback(err, null);
            } else {
                callback(err, id);
            }
        });
    }
};