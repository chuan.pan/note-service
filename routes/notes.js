const express = require('express');
const noteController = require('../controllers/NoteController');
const router = express.Router();

router.get('/', (req, res) => {
    noteController.getNotes((err, notes) => {
        if (err) {
            res.status(err.code || 500).json({message: (err.message || "Internal Server Error")});
        } else {
            res.status(200).json(notes);
        }
    });
});

router.post('/', (req, res) => {
    let payload = req.body;
    let title = (payload.hasOwnProperty('title') && payload.title !== '' && payload.title != null) ? payload.title : "";
    let content = (payload.hasOwnProperty('content') && payload.content !== '' && payload.content != null) ? payload.content : "";
    if (title.length > 255 || content.length > 1024) {
        res.status(400).json({message: "Bad Request"});
    } else {
        noteController.addAndGet(title, content, (err, id) => {
            if (err || id == null) {
                res.status(err.code || 500).json({message: (err.message || "Internal Server Error")});
            } else {
                res.status(200).json({id: id});
            }
        });
    }
});

router.get('/:id', (req, res) => {
    noteController.getNoteByID(req.params.id, (err, note) => {
        if (err) {
            res.status(err.code || 500).json({message: (err.message || "Internal Server Error")});
        } else if (note == null) {
            res.status(404).json({message: "Data not found"});
        } else {
            res.status(200).json(note);
        }
    });
});

router.put('/:id', (req, res) => {
    let payload = req.body;
    if (payload.hasOwnProperty('title') && payload.hasOwnProperty('content')) {
        let title = (payload.title !== '' && payload.title != null) ? payload.title : "";
        let content = (payload.content !== '' && payload.content != null) ? payload.content : "";
        if (title.length > 255 || content.length > 1024) {
            res.status(400).json({message: "Bad Request"});
        } else {
            noteController.updateNoteByID(req.params.id, payload.title, payload.content, (err, result) => {
                if (err) {
                    res.status(err.code || 500).json({message: (err.message || "Internal Server Error")});
                } else if (result == null) {
                    res.status(404).json({message: "Data not found"});
                } else {
                    res.status(200).json({message: "Successful Operation"});
                }
            });
        }
    } else {
        res.status(400).json({message: "Bad Request"});
    }
});
router.delete('/:id', (req, res) => {
    noteController.deleteNoteByID(req.params.id, (err, result) => {
        if (err) {
            res.status(err.code || 500).json({message: (err.message || "Internal Server Error")});
        } else if (result == null) {
            res.status(404).json({message: "Invalid Request"});
        } else {
            res.status(200).json({message: "Successful Operation"});
        }
    });
});

router.get('/:code', (req, res) => {
    noteController.getDataByCode(req.params.code, (err, result) => {
        if (err != null) {
            res.status(500).json({code: 500});
        } else if (result == null) {
            res.status(404).json({code: 404});
        } else {
            let payload = {
                id: result.id,
                shortened_url: req.protocol + '://' + req.headers.host + '/' + req.params.code,
                original_url: result.url
            };
            res.status(200).json(payload);
        }
    });
});

module.exports = router;