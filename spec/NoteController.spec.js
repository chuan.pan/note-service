let note_app_server;
const NoteController = require('../controllers/NoteController');

describe("NoteController", () => {
    let new_node_id;
    beforeAll(() => {
        note_app_server = require('../app.server');
    });

    it("should get new id after add()", () => {
        NoteController.addAndGet("Note1", "Hello!", function (err, id) {
            expect(err).toEqual(null);
            expect(id).not.toEqual(null);
            new_node_id = id;
        });
    });

    it("should return one note after adding a note", () => {
        NoteController.getNotes((err, notes) => {
            expect(err).toEqual(null);
            expect(notes.length).toEqual(1);
        });
    });

    it("should get the note detail by id", () => {
        NoteController.getNoteByID(new_node_id, (err, note) => {
            expect(note.title).toEqual('Note1');
        });
    });

    it("should get the updated values after updating the note", () => {
        NoteController.updateNoteByID(new_node_id, "title-new", "Content-new", (err, id) => {
            expect(err).toEqual(null);
            expect(id).toEqual(new_node_id);
            NoteController.getNoteByID(new_node_id, (err, note) => {
                expect(note.title).toEqual("title-new");
                expect(note.content).toEqual("Content-new");
            });
        });
    });
});